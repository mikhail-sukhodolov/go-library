# Build stage
# указываем базовые образы. golang:1.20-alpine обеспечивает среду выполнения Go и инструменты для сборки приложений на Go.
# AS builder задает имя этой сборочной стадии, которое может быть использовано в дальнейшем при копировании файлов и др.
FROM golang:1.20-alpine AS builder

# устанавливаем каталог /build как текущий рабочий каталог для всех следующих инструкций
WORKDIR /build

# копируем все файлы ИЗ директории, в которой находится Dockerfile ВНУТРЬ контейнера в текущий рабочий каталог
COPY . .

# выполняем загрузку зависимостей проекта внутри сборочной стадии контейнера перед компиляцией приложения
RUN go mod download

#  выполняем сборку Go-приложения внутри сборочной стадии контейнера и сохраняем исполняемый файл в пути /build/library
RUN go build -o /build/library ./cmd/main.go


# Run stage
# используем базовый образ alpine:3.14, который является минимальным образом Alpine Linux версии 3.14.
# он обеспечивает легковесную и минималистичную операционную систему, идеально подходящую для запуска контейнеров
FROM alpine:3.14

# создаем каталог /app и переключаемся на него
WORKDIR /app

#  копируем файл /build/library из сборочной стадии (builder stage) в текущий рабочий каталог внутри контейнера
# после выполнения команды COPY --from=builder /build/library ., данные из /build больше не будут включены в образ, явно удалять их не нужно
COPY --from=builder /build/library .

# копируем swagger.json из сборочной стадии в директорию /app/docs/ внутри контейнера
COPY --from=builder /build/docs/swagger.json ./docs/
COPY --from=builder /build/config/config.yml .
COPY --from=builder /build/config/.env .

#- Слева указывает на путь к файлу или директории на хост-системе. В данном случае, это путь ./config/config.yml относительно местоположения Dockerfile на хост-системе.
#- Справа указывает на путь к файлу или директории внутри контейнера. В данном случае, это путь /config.yml внутри контейнера.
VOLUME ./config/config.yml:/config.yml
VOLUME ./config/.env:/.env

EXPOSE 8080
# каждый раз при запуске контейнера будет запущен исполняемый файл parser
CMD ["/app/library"]
