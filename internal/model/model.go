package model

type Author struct {
	Id         int    `db:"id" json:"id,omitempty" swag:"-"`
	Name       string `db:"name" json:"name" binding:"required"`
	Books      []Book `db:"books" json:"books,omitempty"`
	Popularity int    `db:"popularity" json:"popularity,omitempty" swag:"-"`
}

type Book struct {
	Id         int    `db:"id" json:"id,omitempty" swag:"-"`
	Title      string `db:"title" json:"title" binding:"required"`
	AuthorId   int    `db:"authorid" json:"authorid,omitempty" binding:"required"`
	IsRented   bool   `db:"isrented" json:"isrented,omitempty" swag:"-"`
	UserId     int    `db:"userid" json:"userid,omitempty" swag:"-"`
	Popularity int    `db:"popularity" json:"popularity" swag:"-"`
}

type User struct {
	Id          int    `db:"id" json:"id,omitempty"`
	Name        string `db:"name" json:"name"`
	RentedBooks []Book `db:"rentedbooks" json:"rentedbooks,omitempty"`
}

// swagger structs
type LibraryRequest struct {
	Name  string        `json:"Name"`
	Books []LibraryBook `json:"Books"`
}

type LibraryBook struct {
	Title string `json:"Title"`
}
