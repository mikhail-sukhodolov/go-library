package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// функция newErrorResponse используется для создания и отправки JSON-ответа с заданным статусным кодом и сообщением об ошибке

type ErrorResponse struct {
	message string `json:"message"`
}

func newErrorResponse(c *gin.Context, statusCode int, message string) {
	log.Error().Msg(message)
	c.AbortWithStatusJSON(statusCode, ErrorResponse{message: message})
}
