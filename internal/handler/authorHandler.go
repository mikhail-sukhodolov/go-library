package handler

import (
	"github.com/gin-gonic/gin"
	"go-library/internal/model"
	"net/http"
)

// AUTHOR methods

// AddAuthor
// @Summary      AddAuthor
// @Description  Add new author at library
// @Tags         author
// @Accept       json
// @Produce      json
// @Param   input   body   model.Author   true   "JSON object"
// @Failure      400
// @Failure      500
// @Succes		 200
// @Router       /author [post]
func (h *Handler) AddAuthor(c *gin.Context) {
	// декодируем структуру автора
	var author model.Author
	if err := c.BindJSON(&author); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	// отправляем полученную структуру в метод сервиса
	err := h.service.NewAuthor(author)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}

// ShowAuthors
// @Summary      ShowAuthors
// @Description  Authors list
// @Tags         author
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.Author
// @Router       /author [get]
func (h *Handler) ShowAuthors(c *gin.Context) {
	// получаем структуру или ошибку из метода сервиса
	authors, err := h.service.ShowAuthors()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	// кодируем полученную структуру и отправляем в ответе
	c.JSON(http.StatusOK, authors)
}

// TopAuthors
// @Summary      TopAuthors
// @Description  Top authors list
// @Tags         author
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.Author
// @Router       /author/top [get]
func (h *Handler) TopAuthors(c *gin.Context) {
	// получаем структуру или ошибку из метода сервиса
	topAuthors, err := h.service.TopAuthors()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
	}
	// кодируем полученную структуру и отправляем в ответе
	c.JSON(http.StatusOK, topAuthors)
}
