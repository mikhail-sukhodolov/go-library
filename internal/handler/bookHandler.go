package handler

import (
	"github.com/gin-gonic/gin"
	"go-library/internal/model"
	"net/http"
)

// BOOK methods

// AddBook
// @Summary      AddBook
// @Description  Add new book at library
// @Tags         book
// @Accept       json
// @Produce      json
// @Param   input   body   model.Book   true   "JSON object"
// @Failure      400
// @Failure      500
// @Succes		 200
// @Router       /book [post]
func (h *Handler) AddBook(c *gin.Context) {
	// декодируем json с данными книги
	var book model.Book

	if err := c.BindJSON(&book); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	// отправляем структуру книги в метод сервиса
	err := h.service.NewBook(book)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}

// ShowBooks
// @Summary      ShowBooks
// @Description  Books list
// @Tags         book
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.Book
// @Router       /book [get]
func (h *Handler) ShowBooks(c *gin.Context) {
	// получаем структуру или ошибку из метода сервиса
	books, err := h.service.ShowBooks()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	// кодируем и отправляем ответ
	c.JSON(http.StatusOK, books)
}

// TopBooks
// @Summary      TopBooks
// @Description  Top books list
// @Tags         book
// @Accept       json
// @Produce      json
// @Failure      500
// @Success      200 {object} []model.Book
// @Router       /book/top [get]
func (h *Handler) TopBooks(c *gin.Context) {
	// получаем структуру или ошибку из метода сервиса
	topBooks, err := h.service.TopBooks()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	// кодируем полученную структуру и отправляем ответ
	c.JSON(http.StatusOK, topBooks)
}
