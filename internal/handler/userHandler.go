package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// USER methods

// ShowUsers
// @Summary      ShowUsers
// @Description  Users list
// @Tags         user
// @Accept       json
// @Produce      json
// @Failure 500
// @Success      200 {object} []model.User
// @Router       /user [get]
func (h *Handler) ShowUsers(c *gin.Context) {
	users, err := h.service.ShowUsers()
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	c.JSON(http.StatusOK, users)
}

// RentBook
// @Summary      RentBook
// @Description  Rent book realisation
// @Tags         user
// @Accept       json
// @Produce      json
// @Param id path int true "User ID"
// @Param bookid query int true "Book ID"
// @Success 200
// @Failure 400
// @Failure 500
// @Router       /user/:id/rentbook [post]
func (h *Handler) RentBook(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	bookId, err := strconv.Atoi(c.Query("bookid"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.RentBook(userId, bookId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)

}

// RentBook
// @Summary      ReturnBook
// @Description  Return book realisation
// @Tags         user
// @Accept       json
// @Produce      json
// @Param id path int true "User ID"
// @Param bookid query int true "Book ID"
// @Success 200
// @Failure 400
// @Failure 500
// @Router       /user/:id/returnbook [post]
func (h *Handler) ReturnBook(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	bookId, err := strconv.Atoi(c.Query("bookid"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		if err != nil {
			newErrorResponse(c, http.StatusBadRequest, err.Error())
			return
		}
	}
	err = h.service.ReturnBook(userId, bookId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	c.Status(http.StatusOK)
}
