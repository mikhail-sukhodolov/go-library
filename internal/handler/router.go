package handler

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "go-library/docs"
	"go-library/internal/service"
)

type Handler struct {
	service service.ServiceInterface
}

func NewHandler(service service.ServiceInterface) *Handler {
	return &Handler{service: service}
}

// роутер
func (h *Handler) InitRoutes() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.ErrorLogger())

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	author := r.Group("/author")
	{
		author.POST("/", h.AddAuthor)
		author.GET("/", h.ShowAuthors)
		author.GET("/top", h.TopAuthors)
	}

	book := r.Group("/book")
	{
		book.POST("/", h.AddBook)
		book.GET("/", h.ShowBooks)
		book.GET("/top", h.TopBooks)
	}

	user := r.Group("/user")
	{
		user.GET("/", h.ShowUsers)
		user.POST("/:id/rentbook", h.RentBook)
		user.POST("/:id/returnbook", h.ReturnBook)
	}
	return r
}
