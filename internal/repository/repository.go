package repository

import "go-library/internal/model"

type RepositoryInterface interface {
	AuthorRepositoryInterface
	BookRepositoryInterface
	UserRepositoryInterface
}

type AuthorRepositoryInterface interface {
	AddNewAuthor(model.Author) error
	AllAuthors() ([]model.Author, error)
	TopAuthors() ([]model.Author, error)
}

type BookRepositoryInterface interface {
	AddNewBook(model.Book) error
	AllBooks() ([]model.Book, error)
	TopBooks() ([]model.Book, error)
}

type UserRepositoryInterface interface {
	ShowAllUsers() ([]model.User, error)
	RentBook(int, int) error
	ReturnBook(int, int) error
}
