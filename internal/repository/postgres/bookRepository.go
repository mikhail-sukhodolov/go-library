package postgres

import (
	"fmt"
	"go-library/internal/model"
)

// BOOK methods

func (p *PostgresDB) AddNewBook(book model.Book) error {
	// проверяем существование указанного автора книги
	var exist bool
	err := p.db.Get(&exist, "SELECT EXISTS (SELECT 1 FROM authors WHERE id = $1)", book.AuthorId)
	if err != nil {
		return err
	}
	// если id автора не существует, возвращаем ошибку
	if !exist {
		return fmt.Errorf("author not found")
	}
	// добавляем книгу в таблицу
	_, err = p.db.Exec("INSERT INTO books (id, authorid, title, popularity, userid) VALUES (DEFAULT, $1, $2, $3,$4)", book.AuthorId, book.Title, book.Popularity, book.UserId)
	if err != nil {
		return err
	}
	return nil
}

func (p *PostgresDB) AllBooks() ([]model.Book, error) {
	// выполняем запрос на выюорку нужных столбцов по всем элементам таблицы
	var books []model.Book
	if err := p.db.Select(&books, "SELECT id, title, authorid,popularity FROM books"); err != nil {
		return nil, err
	}
	return books, nil
}

func (p *PostgresDB) TopBooks() ([]model.Book, error) {
	// выполняем запрос на выборку пяти результатов с сортировкой по максимуму по строке countrent
	var books []model.Book
	if err := p.db.Select(&books, "SELECT title, authorid, popularity FROM books ORDER BY popularity DESC LIMIT 5"); err != nil {
		return nil, err
	}
	return books, nil
}
