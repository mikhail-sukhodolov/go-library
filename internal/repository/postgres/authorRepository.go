package postgres

import (
	_ "github.com/lib/pq"
	"go-library/internal/model"
)

// AUTHOR methods

func (p *PostgresDB) AddNewAuthor(author model.Author) error {
	var authorID int
	// вставляем name в таблицу authors, возвращая присвоенный строке индекс
	err := p.db.QueryRow(`INSERT INTO authors (name, popularity) VALUES ($1, $2) RETURNING id`, author.Name, author.Popularity).Scan(&authorID)
	if err != nil {
		return err
	}
	// добавляем книги из среза в таблицу books с привязкой по id
	for _, value := range author.Books {
		_, err := p.db.Exec(`INSERT INTO books(id, title, authorid, popularity) VALUES (DEFAULT, $1, $2, $3)`, value.Title, authorID, value.Popularity)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PostgresDB) AllAuthors() ([]model.Author, error) {
	var authors []model.Author
	// выбираем только нужные столбцы(без среза книг, чтобы избежать ошибки), добавляя в срез авторов
	err := p.db.Select(&authors, `SELECT id, name, popularity FROM authors`)
	if err != nil {
		return nil, err
	}
	// проходим по срезу авторов, делая запрос в таблицу books и добавляем каждому автору книги по его id
	for index := range authors {
		var books []model.Book
		if err := p.db.Select(&books, `SELECT title,popularity FROM books WHERE authorid = $1`, authors[index].Id); err != nil {
			return nil, err
		}
		authors[index].Books = books
	}
	return authors, nil
}

func (p *PostgresDB) TopAuthors() ([]model.Author, error) {
	// выполняем запрос на выборку пяти результатов с сортировкой по максимуму по строке countrent
	var authors []model.Author
	if err := p.db.Select(&authors, "SELECT name, popularity FROM authors ORDER BY popularity DESC LIMIT 5"); err != nil {
		return nil, err
	}
	return authors, nil
}
