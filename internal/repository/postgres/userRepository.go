package postgres

import (
	"fmt"
	"go-library/internal/model"
)

// USER methods

func (p *PostgresDB) ShowAllUsers() ([]model.User, error) {
	// выбираем имена пользователей в срез
	var users []model.User
	err := p.db.Select(&users, "SELECT id, name FROM users")
	if err != nil {
		return nil, err
	}
	// выбираем арендованные книги в срез для каждого пользователя
	for index := range users {
		var books []model.Book
		if err := p.db.Select(&books, "SELECT title, popularity FROM books WHERE userid = $1", users[index].Id); err != nil {
			return nil, err
		}
		users[index].RentedBooks = books
	}
	return users, nil
}

func (p *PostgresDB) RentBook(userId, bookId int) error {
	// проверка на существование пользователя
	var user model.User
	if err := p.db.Get(&user, "SELECT  FROM users WHERE id = $1", userId); err != nil {
		return fmt.Errorf("user not exists")
	}
	// проверяем, существует ли книга
	var book model.Book
	if err := p.db.Get(&book, "SELECT * FROM books WHERE id = $1", bookId); err != nil {
		return fmt.Errorf("no such book")
	}
	// проверяем, арендована ли книга
	if book.IsRented {
		return fmt.Errorf("book already rented")
	}

	// добавляем в книгу userid арендовавшего пользователя, увеличиваем рейтинг книги и отмечаем в книге столбец isRented как true
	_, err := p.db.Exec(`
    UPDATE books
    SET
        userid = $1,
        isrented = true,
        popularity = popularity + 1
    WHERE id = $2`, userId, bookId)
	if err != nil {
		return err
	}

	// увеличиваем рейтинг автора
	if _, err := p.db.Exec("UPDATE authors SET popularity = popularity + 1 WHERE id = $1", book.AuthorId); err != nil {
		return err
	}
	return nil
}

func (p *PostgresDB) ReturnBook(userId, bookId int) error {
	// проверка на существование пользователя
	var user model.User
	if err := p.db.Get(&user, "SELECT FROM users WHERE id = $1", userId); err != nil {
		return fmt.Errorf("user not exists")
	}
	// проверяем, существует ли книга
	var book model.Book
	if err := p.db.Get(&book, "SELECT * FROM books WHERE id = $1", bookId); err != nil {
		return fmt.Errorf("no such book")
	}
	// проверяем, есть ли указанная книга в аренде у пользователя
	if book.UserId != userId {
		return fmt.Errorf("no such book rented by this user")
	}
	// меняем статус книги на  isRented и удаляем userID
	_, err := p.db.Exec("UPDATE books SET userid = 0, isrented = false WHERE id = $1", bookId)
	if err != nil {
		return err
	}
	return nil
}
