package postgres

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"go-library/internal/model"
	"math/rand"
	"os"
	"strings"
)

type PostgresDB struct {
	db *sqlx.DB
}

func NewPosrgresDB() (*PostgresDB, error) {

	// Параметры подключения к базе данных
	connStr := fmt.Sprintf("user=%v password=%v dbname=%v host=%v port=%v sslmode=disable",
		viper.Get("DBUSER"),
		os.Getenv("DBPASSWORD"),
		viper.Get("DBNAME"),
		viper.Get("DBHOST"),
		viper.Get("DBPORT"),
	)

	// Открытие соединения с базой данных
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	// Проверка соединения с базой данных
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	// создаем SQL-запрос для создания таблиц в базе данных PostgreSQL
	// если они еще не существует
	createTableSQL := `
	CREATE TABLE IF NOT EXISTS authors (
		id bigserial PRIMARY KEY,
		name text,
		books JSONB,
		popularity int,
		UNIQUE(name)
	);
	CREATE TABLE IF NOT EXISTS books (
		id bigserial PRIMARY KEY,
		title text,
		authorid int,
		popularity int,
		isrented boolean NOT NULL DEFAULT false,
		userid int,
		UNIQUE(title)
	);	
	CREATE TABLE IF NOT EXISTS users (
		id bigserial PRIMARY KEY,
		name text,
		rentedBooks JSONB
	)`

	if _, err := db.Exec(createTableSQL); err != nil {
		return nil, err
	}

	p := PostgresDB{db}

	// проверяем таблицу authors на пустоту
	var authorsCount int
	_ = db.Get(&authorsCount, "SELECT COUNT(*) FROM authors")
	// заполняем если пустая
	if authorsCount == 0 {
		for i := 1; i <= 50; i++ {
			author := model.Author{
				Name:       gofakeit.FirstName() + " " + gofakeit.LastName(),
				Popularity: 10 + rand.Intn(90),
			}
			err := p.AddNewAuthor(author)
			if err != nil {
				log.Error().Msgf("Error with added authors: %v", err)
			}
		}
	}

	// проверяем таблицу books на пустоту
	var booksCount int
	_ = db.Get(&booksCount, "SELECT COUNT(*) FROM books")
	if booksCount == 0 {
		for i := 1; i <= 100; i++ {
			book := model.Book{
				Title:      strings.Title(gofakeit.HipsterWord() + " " + gofakeit.HipsterWord()),
				AuthorId:   1 + rand.Intn(50),
				Popularity: rand.Intn(10),
				//UserId:     1 + rand.Intn(55),
			}
			err := p.AddNewBook(book)
			if err != nil {
				log.Error().Msgf("Error with added books: %v", err)
			}
		}
	}

	// проверяем таблицу users на пустоту
	var usersCount int
	_ = db.Get(&usersCount, "SELECT COUNT(*) FROM users")
	if usersCount == 0 {
		for i := 0; i <= 55; i++ {
			user := model.User{
				Name: gofakeit.FirstName() + " " + gofakeit.LastName(),
			}
			var userID int
			if err := p.db.QueryRow("INSERT INTO users (name) VALUES ($1) RETURNING id", user.Name).Scan(&userID); err != nil {
				log.Error().Msgf("Error with added users: %v", err)
			}
		}
	}

	return &p, nil
}
