package service

import "go-library/internal/model"

// BOOK methods

func (s *Service) NewBook(book model.Book) error {
	return s.repository.AddNewBook(book)
}

func (s *Service) ShowBooks() ([]model.Book, error) {
	return s.repository.AllBooks()
}

func (s *Service) TopBooks() ([]model.Book, error) {
	return s.repository.TopBooks()
}
