package service

import "go-library/internal/model"

// USER methods

func (s *Service) ShowUsers() ([]model.User, error) {
	return s.repository.ShowAllUsers()
}

func (s *Service) RentBook(userId, bookId int) error {
	return s.repository.RentBook(userId, bookId)
}

func (s *Service) ReturnBook(userId, bookId int) error {
	return s.repository.ReturnBook(userId, bookId)
}
