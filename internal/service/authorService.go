package service

import "go-library/internal/model"

// AUTHOR methods

func (s *Service) NewAuthor(author model.Author) error {
	return s.repository.AddNewAuthor(author)
}

func (s *Service) ShowAuthors() ([]model.Author, error) {
	return s.repository.AllAuthors()
}

func (s *Service) TopAuthors() ([]model.Author, error) {
	return s.repository.TopAuthors()
}
