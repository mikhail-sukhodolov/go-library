package service

import (
	"go-library/internal/model"
	"go-library/internal/repository"
)

type Service struct {
	repository repository.RepositoryInterface
}

func NewService(repository repository.RepositoryInterface) *Service {
	return &Service{repository: repository}
}

type ServiceInterface interface {
	AuthorServiceInterface
	BookServiceInterface
	UserServiceInterface
}

type BookServiceInterface interface {
	NewBook(model.Book) error
	ShowBooks() ([]model.Book, error)
	TopBooks() ([]model.Book, error)
}

type AuthorServiceInterface interface {
	NewAuthor(model.Author) error
	ShowAuthors() ([]model.Author, error)
	TopAuthors() ([]model.Author, error)
}

type UserServiceInterface interface {
	ShowUsers() ([]model.User, error)
	RentBook(int, int) error
	ReturnBook(int, int) error
}
