package main

import (
	"context"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	_ "go-library/docs"
	"go-library/internal/handler"
	"go-library/internal/repository"
	"go-library/internal/repository/postgres"
	"go-library/internal/service"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {

	// @title           Library service
	// @version         1.0
	// @description     API server for realize library service
	// @host     		postgres

	// загружаем конфиг
	viper.SetConfigFile("config.yml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("config load error")
	}

	// загружаем .env для безопасного хранения паролей
	if err = godotenv.Load(".env"); err != nil {
		log.Fatal().Err(err).Msg(".env load error")
	}

	// выбираем и грузим бд
	dbType := viper.Get("DBDRIVER")
	var repo repository.RepositoryInterface

	switch dbType {
	case "postgres":
		repo, err = postgres.NewPosrgresDB()
		if err != nil {
			log.Fatal().Err(err).Msg("postgres create error")
		}
	case "mongodb":
		log.Info().Msg("implement me")
	default:
		log.Fatal().Err(err).Msg("unknown database type")
	}
	// создаем экземпляры сервиса и хендлера
	serv := service.NewService(repo)
	hand := handler.NewHandler(serv)

	// запускаем сервер
	server := http.Server{
		Addr:         ":8080",
		Handler:      hand.InitRoutes(),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	go func() {
		log.Info().Msgf("starting server at port %v", server.Addr)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msgf("error with starting server: %v", err)
		}
	}()

	// останавливаем сервер
	quit := make(chan os.Signal, 1)
	// обработчик для сигнала, ждем interrupt в канале
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Info().Msg("shutdown server...")

	// таймаут завершения
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Err(err).Msgf("server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {
			log.Error().Err(err).Msg("server close error")
		}
	}(&server)
}
